register 'piggybank-0.12.0.jar';

--startup command (local)
/*
pig -x local -f wager_amount_count.pig -param inputFile=tmp.csv -param outputFolder=output/wager_amount_count/
*/

--Load data from games log
/*
Game UID;
Partner Name;
Game System;
Player ID;
Game Client;
Date;
Gross Gaming;
Gross Gaming Revenue;
Jackpot Winnings;
No of Game Rounds;
No of Wagers;
No of Winnings;
Total Jackpot Contributions;
Total Wager Amount;
Wager Real Amount;
Total Win Amount;
Win Real Amount
*/
session_rows = LOAD '$inputFile' USING org.apache.pig.piggybank.storage.CSVExcelStorage(';') AS 
(GameUID:chararray, 
PartnerName:chararray,
GameSystem:chararray,
PlayerID:chararray,
GameClient:chararray, 
Date:chararray, 
GrossGaming:chararray, 
GrossGamingRevenue:chararray, 
JackpotWinnings:chararray,
GameRoundsNumber:int, 
WagersNumber:int, 
WinningsNumber:chararray, 
TotalJackpotContributions:chararray, 
TotalWagerAmount:chararray, 
WagerRealAmount:float,
TotalWinAmount:chararray, 
WinRealAmount:chararray); 

--Here we could use FILTER function to select lines of the last 30 days

--Projection to work only with needed columns
session_rows = FOREACH session_rows GENERATE GameUID, PlayerID, WagersNumber, WagerRealAmount;

--ROUND TOTAL BY GameUID && PlayerID
session_rows_grouped = GROUP session_rows BY (GameUID, PlayerID);
session_rows_grouped_count = FOREACH session_rows_grouped GENERATE FLATTEN(group), SUM(session_rows.WagersNumber) as wager_count, SUM(session_rows.WagerRealAmount) as wager_amount;

store session_rows_grouped_count into '$outputFolder' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE');



